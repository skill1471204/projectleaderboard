#include <iostream>
#include <string>

class player
{
private:
	std :: string name;
	

public:

	int score;

	void SetScore(int Newscore)
	{
		score = Newscore;
	}

	void SetName(std::string String)
	{
		name = String;
	}

	int GetScore() 
	{		
		return score;
	}

	std::string GetName() const
	{
		return name;	
	}
};

int main()
{
	int N;
	std::cout << "Enter number of players ";
	std::cin >> N;
	int* scores = new int[N];
	std::string* names = new std::string[N];
	player* Players = new player[N];

	for (int i(0); i < N; i++)
	{
		std::cout << "Enter the name of player " << i + 1 << "\n";
		std::cin >> names[i];
	}
		
	for (int i = 0; i < N; i++) 
	{
		std::cout << "Enter the score of player " << names[i] << "\n";
		std::cin >> scores [i];

	}
	
	for (int i(0); i < N; i++)
	{
		Players[i].SetName(names[i]);
		Players[i].SetScore(scores[i]);
	}
	
	for (int i = 0; i < N; ++i)
	{
		for (int j = N - 1; j > i; j--)
		{
			if ((Players[j].score) > (Players[j - 1].score))
			{
				std::swap(Players[j], Players[j - 1]);
			}
		}
	}

	for (int i = 0; i < N; i++)
	{
		std::cout << Players[i].GetName() << " " << Players[i].GetScore() << "\n";
		

	}
	
}
